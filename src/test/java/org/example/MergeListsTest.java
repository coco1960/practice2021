package org.example;

import org.junit.Test;

import static org.junit.Assert.*;

public class MergeListsTest {
    MergeLists llist1 = new MergeLists();
    MergeLists llist2 = new MergeLists();
    MergeLists llist3 = new MergeLists();

    @Test
    public void sortedMerge() {
        // Node head1 = new Node(5);
        llist1.addToTheLast(new Node(5));
        llist1.addToTheLast(new Node(10));
        llist1.addToTheLast(new Node(15));

        // Node head2 = new Node(2);
        llist2.addToTheLast(new Node(2));
        llist2.addToTheLast(new Node(3));
        llist2.addToTheLast(new Node(20));

      llist1.head = llist1.sortedMerge(llist1.head, llist2.head);
       assertTrue(llist1.head.data == 2);
       //list3.head = llist1.mergeList(llist1.head,llist2.head);
       //ssertTrue(llist3.head.data == 2);
      //  llist1.deleteNode(5);
      //  assertTrue(llist1.head.next.data == 15);
    }

    @Test
    public void rotate() {
        llist2.addToTheLast(new Node(2));
        llist2.addToTheLast(new Node(3));
        llist2.addToTheLast(new Node(4));
        llist2.addToTheLast(new Node(5));
        llist2.addToTheLast(new Node(6));
        llist2.addToTheLast(new Node(7));
        //llist2.head = llist2.reverse(llist2.head, 3);
        //assertTrue(llist2.head.data == 2);
        llist2.rotate(4);
        assertTrue(llist2.head.next.next.next.next.next.next == null);
    }

    @Test
    public void findMergingPoint() {
        llist2.addToTheLast(new Node(2));
        llist2.addToTheLast(new Node(3));
        llist2.addToTheLast(new Node(4));
        llist2.addToTheLast(new Node(5));
        llist2.addToTheLast(new Node(6));
        llist2.addToTheLast(new Node(16));

        llist1.addToTheLast(new Node(1));
        llist1.addToTheLast(new Node(5));
        llist1.addToTheLast(new Node(10));
        llist1.addToTheLast(new Node(15));
        Node n = llist2.findMergingPoint(llist1.head, llist2.head);
        assertTrue( n.data == 5);
    }

    @Test
    public void findBegLoop() {
    }

    @Test
    public void checkPalindrome() {
        llist2.addToTheLast(new Node(2));
        llist2.addToTheLast(new Node(3));
        llist2.addToTheLast(new Node(4));
        llist2.addToTheLast(new Node(4));
        llist2.addToTheLast(new Node(3));
        llist2.addToTheLast(new Node(2));
        boolean p = llist2.checkPalindrome();
        assertTrue(p == true);
    }

    @Test
    public void removeLoop() {
        llist2.addToTheLast(new Node(2));
        llist2.addToTheLast(new Node(3));
        Node nodeToLoop = new Node((4));
        llist2.addToTheLast(nodeToLoop);
        llist2.addToTheLast(new Node(5));
        llist2.addToTheLast(new Node(6));
        Node lastNode = new Node(7);
        llist2.addToTheLast(lastNode);
        lastNode.next = nodeToLoop;
        Node beginLoop = llist2.findBegLoop();
        assertTrue(beginLoop.data == 4);
        llist2.removeLoop();
        assertTrue(lastNode.next == null);
    }

    @Test
    public void testFindBegLoop() {
        llist2.addToTheLast(new Node(2));
        llist2.addToTheLast(new Node(3));
        Node nodeToLoop = new Node((4));
        llist2.addToTheLast(nodeToLoop);
        llist2.addToTheLast(new Node(5));
        llist2.addToTheLast(new Node(6));
        Node lastNode = new Node(7);
        llist2.addToTheLast(lastNode);
        lastNode.next = nodeToLoop;
        Node beginLoop = llist2.findBegLoop();
        assertTrue(beginLoop.data == 4);
    }

    @Test
    public void pairwise() {
        llist2.addToTheLast(new Node(2));
        llist2.addToTheLast(new Node(3));
        llist2.addToTheLast(new Node(4));
        llist2.addToTheLast(new Node(5));
        llist2.addToTheLast(new Node(6));
        llist2.addToTheLast(new Node(7));
        llist2.Pairwise();
        assertTrue(llist2.head.next.next.data == 5);
    }

    @Test
    public void divideList() {
        llist2.addToTheLast(new Node(4));
        llist2.addToTheLast(new Node(5));
        llist2.addToTheLast(new Node(7));
        llist2.addToTheLast(new Node(6));
        llist2.addToTheLast(new Node(3));
        llist2.addToTheLast(new Node(2));
        Node result = llist2.divideList(4);
        assertTrue(result.next.next.next.data == 5);
    }
}