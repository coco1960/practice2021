package org.example;

/* Java program to merge two
sorted linked lists */
import java.util.*;

/* Link list node */
class Node
{
    int data;
    Node next;
    Node(int d) {data = d;
        next = null;}
}

class MergeLists
{
    Node head;

public Node divideList( int val)
{
    Node first,  second, current,  nextCurrent,  mid,tail1,tail2;
    first = null;
    second = null;
    mid = null;

    tail1 = null;
    tail2 = null;
    current = head;
    while (current != null) {
        nextCurrent = current.next;
        if (current.data < val) {
            if(first == null){
                first = current;
                tail1 = first;
                current.next = null;
            }
            else{
                tail1.next = current;
                tail1 = tail1.next;
                current.next = null;
            }
        }
        else if (current.data > val) {
            if(second == null){
                second = current;
                tail2 = second;
                current.next = null;
            }
            else{
                tail2.next = current;
                tail2 = tail2.next;
                current.next = null;
            }
        }
        else {
            current.next = mid;
            mid = current;
            current.next = null;
        }
        current = nextCurrent;
    }
    if (mid != null) {
        tail1.next = mid;
        tail1 = tail1.next;
    }
    if(second != null){
        tail1.next = second;
    }
    return first;
}
    public void Pairwise(){
        Node temp = head;
        while(temp != null && temp.next != null){
            int value = temp.data;
            temp.data = temp.next.data;
            temp.next.data = value;
            temp = temp.next.next;
        }
    }
public Node findMiddle()
{
    Node slow = head;
    Node fast = head;

    while (fast != null && fast.next != null)
    {
        fast = fast.next.next;
        slow = slow.next;
    }
    return slow;
}
    public boolean checkPalindrome() {
    boolean pal = true;
    Node slow, fast;
    Stack<Integer>myStack = new Stack<>();
    slow = head;
    fast = head;
    while (fast.next!= null && fast.next.next!= null) {
        myStack.push(slow.data);
        slow = slow.next;
        fast = fast.next.next;
    }
    if (fast.next == null) {////for odd list
        slow = slow.next;
        while (slow != null) {
            if (slow.data != myStack.peek ()) {
                pal = false;
                break;
            }
            slow = slow.next;
            myStack.pop();
        }
    }
    else {
        myStack.push(slow.data);//////for even list
        slow = slow.next;
        while (slow != null) {
            if (slow.data != myStack.peek ()) {
                pal = false;
                break;
            }
            slow = slow.next;
            myStack.pop();
        }
    }
    return pal;
}
    public void removeLoop() {
        Node prev, prevCurrent,current;
        prev = head.next;
        prevCurrent = head.next;
        current = head.next.next;
        while (prev != current) {
            prev = prev.next;
            prevCurrent = prevCurrent.next.next;
            current = current.next.next;
        }
        prev = head;
        while (prev != current) {
            prev = prev.next;
            prevCurrent = prevCurrent.next;
            current = current.next;
        }
        prevCurrent.next = null;
    }
public Node findBegLoop() {
    Node prev, current;
    prev = head.next;
    current = head.next.next;
    while (prev != current) {
        prev = prev.next;
        current = current.next.next;
    }
    prev = head;
    while (prev != current) {
        prev = prev.next;
        current = current.next;
    }
    return prev;
    }
public Node findMergingPoint(Node a, Node b) {
    Node temp1 = a,  temp2 = b;
    int counter1 = 0, counter2 = 0;
    while (temp1 != null) {
        temp1 = temp1.next;
        counter1++;
    }
    while (temp2 != null) {
        temp2 = temp2.next;
        counter2++;
    }
    temp1 = a;
    temp2 = b;
    if (counter1 >= counter2) {
        counter1 -=counter2;
        while (counter1 > 0) {
            temp1 = temp1.next;
            counter1--;
        }
    }
    else {
        counter2 -= counter1;
        while (counter2 > 0) {
            temp2 = temp2.next;
            counter2--;
        }
    }
    while (temp1 != null) {
        if (temp1.data == temp2.data) {
            return temp1;
        }
        temp1 = temp1.next;
        temp2 = temp2.next;
    }
    return null;
}
//**************************************
    public  Node reverse(Node head, int k)
    {
        if(head == null)
            return null;
        Node current = head;
        Node next = null;
        Node prev = null;

        int count = 0;

        /* Reverse first k nodes of linked list */
        while (count < k && current != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
            count++;
        }

        /* next is now a pointer to (k+1)th node
           Recursively call for the list starting from
           current. And make rest of the list as next of
           first node */
        if (next != null)
            head.next = reverse(next, k);

        // prev is now head of input list
        return prev;
    }
    //**************************************************************
public void rotate(int rot){
    Node current = head;
    while(current.next != null){
        current = current.next;
    }
    while (rot > 0){
        current.next = head;
        current = current.next;
        head = head.next;
        current.next = null;
        rot--;
    }
}
//**********************************************************
    public void deleteNode(int value){
        Node current = head;
        while(current.next != null){
            if(current.data == value){
                current.data = current.next.data;
                current.next = current.next.next;
                break;
            }
        }
    }
    /* Method to insert a node at
    the end of the linked list */
    public void addToTheLast(Node node)
    {
        if (head == null)
        {
            head = node;
        }
        else
        {
            Node temp = head;
            while (temp.next != null)
                temp = temp.next;
            temp.next = node;
        }
    }

    /* Method to print linked list */
    void printList()
    {
        Node temp = head;
        while (temp != null)
        {
            System.out.print(temp.data + " ");
            temp = temp.next;
        }
        System.out.println();
    }
    Node sortedMerge(Node headA, Node headB)
    {

	/* a dummy first node to
	hang the result on */
        Node dummyNode = new Node(0);

	/* tail points to the
	last result node */
        Node tail = dummyNode;
        while(true)
        {

		/* if either list runs out,
		use the other list */
            if(headA == null)
            {
                tail.next = headB;
                break;
            }
            if(headB == null)
            {
                tail.next = headA;
                break;
            }

		/* Compare the data of the two
		lists whichever lists' data is
		smaller, append it into tail and
		advance the head to the next Node
		*/
            if(headA.data <= headB.data)
            {
                tail.next = headA;
                headA = headA.next;
            }
            else
            {
                tail.next = headB;
                headB = headB.next;
            }

            /* Advance the tail */
            tail = tail.next;
        }
        dummyNode =  dummyNode.next;
        return dummyNode;
       // return dummyNode.next;
    }
////
    Node mergeList(Node first, Node sec) {
        Node lastSmall; //pointer to the last node of						//the merged list
        Node newHead; //pointer to the merged list
        if (first == null) //the first sublist is empty
            return sec;
        else if (sec == null) //the second sublist is empty
            return first;
        else {
            if (first.data < sec.data) //compare the first nodes
            {
                newHead = first;
                lastSmall = first;
                first = first.next;
            } else {
                newHead = sec;
                lastSmall = sec;
                sec = sec.next;
            }
            while (first != null && sec != null) {
                if (first.data < sec.data) {
                    lastSmall.next = first;
                    lastSmall = lastSmall.next;
                    first = first.next;
                } else {
                    lastSmall.next = sec;
                    lastSmall = lastSmall.next;
                    sec = sec.next;
                }
            } //end while
            if (first == null) //first sublist is exhausted first
                lastSmall.next = sec;
            else //second sublist is exhausted first
                lastSmall.next = first;
            return newHead;
        }
    }

    /////
    // Driver Code
    public static void main(String args[])
    {
	/* Let us create two sorted linked
	lists to test the methods
	Created lists:
		llist1: 5->10->15,
		llist2: 2->3->20
	*/
        MergeLists llist1 = new MergeLists();
        MergeLists llist2 = new MergeLists();

        // Node head1 = new Node(5);
        llist1.addToTheLast(new Node(5));
        llist1.addToTheLast(new Node(10));
        llist1.addToTheLast(new Node(15));

        // Node head2 = new Node(2);
        llist2.addToTheLast(new Node(2));
        llist2.addToTheLast(new Node(3));
        llist2.addToTheLast(new Node(20));

       MergeLists llist3 = new MergeLists();
       // llist3.head = llist1.mergeList(llist1.head, llist2.head);
       // llist3.printList();
        llist1.printList();
        llist2.printList();
        llist1.head = llist1.sortedMerge(llist1.head, llist2.head);
        llist1.printList();

    }
}
